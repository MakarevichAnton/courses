﻿using FluentMigrator;

namespace Data.Migrations
{
    [Migration(2013072401)]
    public class BaseMigration : Migration
    {
        public override void Up()
        {
            Create.Table("Company")
                  .WithColumn("Id").AsGuid().PrimaryKey()
                  .WithColumn("Name").AsString();           
        }

        public override void Down()
        {

        }     
    }
}
